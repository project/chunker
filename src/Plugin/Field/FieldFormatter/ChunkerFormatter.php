<?php

namespace Drupal\chunker\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'Chunker' formatter.
 *
 * Divide long HTML content into sections based on h2 headings.
 *
 * Mostly lifted from Drupal 7 chunker module by dman.
 *
 * @FieldFormatter(
 *   id = "chunker",
 *   label = @Translation("Chunker"),
 *   field_types = {
 *     "text_long",
 *     "text_with_summary"
 *   }
 * )
 */
class ChunkerFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'start_level' => 2,
      'section_tag' => 'div',
      'section_class' => 'chunker-section',
      'permalink_string' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['start_level'] = [
      '#title' => $this->t('Start level'),
      '#description' => $this->t('What HTML heading level to enwrap as section. Will work upwards to h1.'),
      '#type' => 'select',
      '#options' => [
        2 => 2,
        3 => 3,
      ],
      '#default_value' => $this->getSetting('start_level'),
    ];

    $form['section_tag'] = [
      '#title' => $this->t('Section tag'),
      '#type' => 'select',
      '#options' => ['div' => 'div'],
      '#default_value' => $this->getSetting('section_tag'),
    ];

    $form['section_class'] = [
      '#title' => $this->t('Section class'),
      '#description' => $this->t('Class to assign to section tag.'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('section_class'),
      '#max_length' => 32,
    ];

    $form['permalink_string'] = [
      '#title' => $this->t('Permalink string'),
      '#description' => $this->t('Leave blank for no permalink. Only works on level(s) being chunked'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('permalink_string'),
      '#max_length' => 32,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $summary[] = $this->t('Chunks text by h@start_level headings', [
      '@start_level' => $this->getSetting('start_level'),
    ]);
    $summary[] = $this->t('Section tag: @section_tag', [
      '@section_tag' => $this->getSetting('section_tag'),
    ]);
    $summary[] = $this->t('Section class: @section_class', [
      '@section_class' => $this->getSetting('section_class'),
    ]);

    $permalink_string = $this->getSetting('permalink_string') ?
      $this->getSetting('permalink_string') : '<none>';
    $summary[] = $this->t('Permalink string: @permalink_string', [
      '@permalink_string' => $permalink_string,
    ]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $settings = $this->getSettings();

    // Treat as for default formatter, but we chunk the HTML.
    // The ProcessedText element already handles cache context & tag bubbling.
    // @see \Drupal\filter\Element\ProcessedText::preRenderText()
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#type' => 'processed_text',
        '#text' => $this->chunkText($item->value, $settings),
        '#format' => $item->format,
        '#langcode' => $item->getLangcode(),
      ];
    }

    return $elements;
  }

  /**
   * Actually chunk the text, starting with HTML, ending with HTML.
   *
   * Goes in and out of XML, so may add some artifacts, but uses the methods
   * from filter.module so we expect it to be safe.
   *
   * @param string $text
   *   HTML to be chunked.
   * @param array $settings
   *   Additional parameters, such as depth or classnames.
   *
   * @return string
   *   Rebuilt markup.
   */
  public function chunkText($text, array $settings) {

    $dom = Html::load($text);
    // Html load wraps text in <body>, so lets pull out the body element.
    $bodies = $dom->getElementsByTagName('body');
    /** @var \DOMElement $scope */
    $scope = $bodies->item(0);

    // To get it all sorted, recursively re-run the process for several depths.
    // First enwrap the h3s, then the h2s, then the h1s.
    for ($recurse = $settings['start_level']; $recurse > 1; $recurse--) {
      $this->enwrap($scope, $recurse, $settings);
    }

    return Html::serialize($dom);
  }

  /**
   * Create sections around document contents based on headings, e.g. h2s.
   */
  public function enwrap($scope, $start_level, array $settings) {

    $settings += $this->defaultSettings();

    $dom = $scope->ownerDocument;

    $heading_tag = "h$start_level";
    // Need to make sure I don't chunk above my requested level.
    // List things that should break my flow.
    $higher_tags = [];
    for ($t = $start_level - 1; $t > 0; $t--) {
      $higher_tags["h$t"] = "h$t";
    }

    // Check each element that is a direct child of the scope.
    // Pick them off one by one and sort them into more organized boxes.
    $current_section_content = $scope;

    // Jiggling the domnodelist in realtime confuses PHP tragically.
    // Enumerate the child nodes into a simpler array before moving them around.
    $child_nodes = [];
    foreach ($scope->childNodes as $item) {
      $child_nodes[] = $item;
    }
    foreach ($child_nodes as $item) {
      if (!empty($higher_tags[$item->nodeName])) {
        // If I find higher tags than expected, stop what I'm doing and
        // close the current container. That prevents H2s from swallowing H1s
        // etc. This makes it safe to run through twice to create nesting.
        $current_section_content = clone $scope;
        $current_section_content->appendChild($item);
      }
      else {
        if ($item->nodeName == $heading_tag) {
          // So we have found $item is a heading.
          if (isset($section_index)) {
            $section_index++;
          }
          else {
            $section_index = 1;
          }
          // If the editor chose their own ID, respect that.
          if (($existing_id = $item->getAttribute('id'))) {
            $section_id = $existing_id;
          }
          else {
            // Else generate own id.
            $section_id = 'section-' . $section_index;
            // Generate meaningful ids, but ensure uniqueness.
            // $section_id = Html::cleanCssIdentifier($item->nodeValue);.
            $item->setAttribute('id', $section_id);
          }

          if (!empty($settings['section_tag'])) {
            $current_section = $dom->createElement($settings['section_tag']);
            $current_section->setAttribute('class', $settings['section_class']);
            $scope->appendChild($current_section);
          }
          else {
            // An empty section tag means skip the section wrapper.
            // This also means that the current section remains just the scope,
            // as that's where things get added.
            $current_section = $scope;
          }
          $current_section->appendChild($item);

          // Optionally, add an anchor link to the heading.
          if (!empty($settings['permalink_string'])) {
            $permalink = $dom->createElement('a', $settings['permalink_string']);
            $permalink->setAttribute('href', '#' . $section_id);
            $item->appendChild($dom->createTextNode(' '));
            $item->appendChild($permalink);
            $permalink->setAttribute('class', 'permalink');
          }

          if (!empty($settings['heading_class'])) {
            $heading_class = implode(' ', [
              $settings['heading_class'],
              $item->getAttribute('class'),
            ]);
            $item->setAttribute('class', $heading_class);
          }

        }
        else {
          // It's just content, add it to the current context.
          if (!empty($current_section)) {
            $current_section->appendChild($item);
          }
          else {
            $current_section_content->appendChild($item);
          }
        }
      }
    }
  }

}
