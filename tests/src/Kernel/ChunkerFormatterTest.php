<?php

namespace Drupal\Tests\chunker\Kernel;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\filter\Entity\FilterFormat;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Tests the formatter functionality.
 *
 * @group chunker
 */
class ChunkerFormatterTest extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['chunker', 'text'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    FilterFormat::create([
      'format' => 'full_html',
      'name' => 'Full HTML',
      'filters' => [],
    ])->save();

    FieldStorageConfig::create([
      'field_name' => 'formatted_text',
      'entity_type' => 'entity_test',
      'type' => 'text_long',
      'settings' => [],
    ])->save();

    FieldConfig::create([
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
      'field_name' => 'formatted_text',
      'label' => 'Filtered text',
    ])->save();
  }

  /**
   * Test the formatter.
   */
  public function testFormatter() {
    $content = <<<HTML
<h2 id="s1">H2 Section 1</h2>
<p>Content section 1</p>
<h2 id="s2">H2 Section 2</h2>
<p>Content section 2</p>
<h2 id="s3">H2 Section 3</h2>
<p>Content section 3</p>
<h2 id="s4">H2 Section 4</h2>
<p>Content section 4</p>
<h2 id="s5">H2 Section 5</h2>
<p>Content section 4</p>
HTML;

    // Create the entity.
    $entity = $this->container->get('entity_type.manager')
      ->getStorage('entity_test')
      ->create(['name' => $this->randomMachineName()]);
    $entity->formatted_text = [
      'value' => $content,
      'format' => 'full_html',
    ];
    $entity->save();

    // Test default settings.
    $build = $entity->get('formatted_text')->view(['type' => 'chunker']);
    \Drupal::service('renderer')->renderRoot($build[0]);

    $expected = <<<HTML
<div class="chunker-section"><h2 id="s1">H2 Section 1</h2>
<p>Content section 1</p>
</div><div class="chunker-section"><h2 id="s2">H2 Section 2</h2>
<p>Content section 2</p>
</div><div class="chunker-section"><h2 id="s3">H2 Section 3</h2>
<p>Content section 3</p>
</div><div class="chunker-section"><h2 id="s4">H2 Section 4</h2>
<p>Content section 4</p>
</div><div class="chunker-section"><h2 id="s5">H2 Section 5</h2>
<p>Content section 4</p></div>
HTML;

    $this->assertEquals($expected, (string) $build[0]['#markup']);

    // Test with permalink, alternate class and alternate tag.
    $build = $entity->get('formatted_text')->view([
      'type' => 'chunker',
      'settings' => [
        'start_level' => '2',
        'section_tag' => 'section',
        'section_class' => 'document-section',
        'permalink_string' => '#',
      ],
    ]);
    \Drupal::service('renderer')->renderRoot($build[0]);

    $expected = <<<HTML
<section class="document-section"><h2 id="s1">H2 Section 1 <a href="#s1" class="permalink">#</a></h2>
<p>Content section 1</p>
</section><section class="document-section"><h2 id="s2">H2 Section 2 <a href="#s2" class="permalink">#</a></h2>
<p>Content section 2</p>
</section><section class="document-section"><h2 id="s3">H2 Section 3 <a href="#s3" class="permalink">#</a></h2>
<p>Content section 3</p>
</section><section class="document-section"><h2 id="s4">H2 Section 4 <a href="#s4" class="permalink">#</a></h2>
<p>Content section 4</p>
</section><section class="document-section"><h2 id="s5">H2 Section 5 <a href="#s5" class="permalink">#</a></h2>
<p>Content section 4</p></section>
HTML;

    $this->assertEquals($expected, (string) $build[0]['#markup']);
  }

}
