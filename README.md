CONTENTS OF THIS FILE
---------------------

* Introduction
* Recommended modules
* Installation
* Configuration
* Troubleshooting
* Maintainers

INTRODUCTION
------------

A very basic successor to the Drupal 7 Chunker module by dman. See https://www.drupal.org/project/chunker/.
D7 chunker was implemented as a text filter. In Drupal 9, it is implemented as a field formatter.

For large HTML documents, the chunker can inject div wrappers to create content sections based on h2 headings within the markup.

This allows other modules to provide navigation (e.g. previous/next nav on sections), or collapse page content into an accordion or similar widget.

Changes from Drupal 7:
* implemented as a field formatter rather than a text filter.
* accordion and similar markup not supported.
* Changing heading tags to a different element is not supported.
* Chunking at level 3 untested.

RECOMMENDED MODULES
-------------------

* Consider Sector ToC and/or Sector Multipage.

INSTALLATION
------------

* Install as usual, see
  https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure-drupal-8 for further
  information.

CONFIGURATION
-------------

* For a content type, choose the 'Chunker' formatter in the Manage display form.

TROUBLESHOOTING
---------------

* Assumes headings in long text field are not already wrapped in div or similar elements.

MAINTAINERS
-----------

Current maintainers: unknown.
